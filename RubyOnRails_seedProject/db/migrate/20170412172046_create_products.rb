class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.integer :sale_id
      t.string :product_name
      t.float :amount
      t.float :quantity
      t.string :unity

      t.timestamps
    end
  end
end
