class AddNameProfileIdToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :nameProfile_id, :integer
    add_index :users, :nameProfile_id
  end
end
