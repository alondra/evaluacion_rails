# This file should contain all the record creation needed to seed the database
# with its default values.
# The data can then be loaded with the rails db:seed command (or created
# alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create(
#     [{ name: 'Star Wars' }, { name: 'Lord of the Rings' }]
#   )
#   Character.create(name: 'Luke', movie: movies.first)

User.create(
  email: 'smartadmin@example.org',
  password: 'smartadmin'
)




tienda1 = Shop.new
tienda1.name = "Norte"
tienda1.save

tienda2 = Shop.new
tienda2.name = "Poniente"
tienda2.save

tienda3 = Shop.new
tienda3.name = "Bajio"
tienda3.save

pos1 = Pos.new
pos1.shop_id = tienda1.id
pos1.save

pos2 = Pos.new
pos2.shop_id = tienda2.id
pos2.save

pos3 = Pos.new
pos3.shop_id = tienda3.id
pos3.save

sales1 = Sale.new
sales1.pos_id  = pos1.id
sales1.save

sales2 = Sale.new
sales2.pos_id = pos2.id
sales2.save

sales3 = Sale.new
sales3.pos_id = pos3.id
sales3.save


