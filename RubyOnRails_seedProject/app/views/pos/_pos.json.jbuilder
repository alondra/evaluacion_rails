json.extract! pos, :id, :shop_id, :number, :created_at, :updated_at
json.url pos_url(pos, format: :json)
