class ProfilesController < ApplicationController
  before_action :set_profile, only: [:show, :edit, :update, :destroy]

  @@prof = "Profiles"

  # GET /profiles
  # GET /profiles.json
  def index


    @profiles = Profile.all

    if current_user.nameProfile_id != 0
      @per = Profile.where("nameProfile_id = ?", current_user.nameProfile_id)
      @per.each do |permisos|
        if permisos.name == @@prof.to_s
          @uno = permisos.name
          @@crear = permisos.crear
          @@editar = permisos.editar
          @@leer = permisos.leer
          @@eliminar = permisos.eliminar

          @crear = permisos.name
          @editar = permisos.editar
          @leer = permisos.leer
          @eliminar = permisos.eliminar
        end
      end
      if ((current_user.nameProfile_id == 1)||(@@leer == 2))
        redirect_to home_index_path
      else
        redirect_to home_index_path, :alert => "Ask for Permissions!"
      end
    else
      redirect_to home_index_path, :alert => "Ask for Permissions!"
    end

  end

  # GET /profiles/1
  # GET /profiles/1.json
  def show

    @crear = @@crear
    @editar = @@editar
    @leer = @@leer
    @eliminar = @@eliminar

    if current_user.nameProfile_id != 0
      if ((current_user.nameProfile_id == 1) || (@@leer == 2))
        redirect_to home_index_path
      else
        redirect_to home_index_path, :alert => "Ask for Permissions!"
      end
    else
      redirect_to home_index_path, :alert => "Ask for Permissions!"
    end
  end

  # GET /profiles/new
  def new
    if current_user.nameProfile_id !=0
      if ((current_user.nameProfile_id == 1)||(@@crear == 8))
        redirect_to home_index_path
      else
        redirect_to home_index_path, :alert => "Ask for Permissions!"
      end
    else
      redirect_to home_index_path, :alert => "Ask for Permissions!"
    end
  end

  # GET /profiles/1/edit
  def edit
    if current_user.nameProfile_id !=0
      if ((current_user.nameProfile_id == 1)||(@@editar == 4))
        redirect_to home_index_path
      else
        redirect_to home_index_path, :alert => "Ask for Permissions!"
      end
    else
      redirect_to home_index_path, :alert => "Ask for Permissions!"
    end
  end

  # POST /profiles
  # POST /profiles.json
  def create
    @profile = Profile.new(profile_params)

    @profile.crear = params[:crear];
    @profile.editar = params[:editar];
    @profile.leer = params[:leer];
    @profile.eliminar = params[:eliminar];
    @profile.total = params[:total];


    respond_to do |format|
      if @profile.save
        format.html { redirect_to @profile, notice: 'Profile was successfully created.' }
        format.json { render :show, status: :created, location: @profile }
      else
        format.html { render :new }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /profiles/1
  # PATCH/PUT /profiles/1.json
  def update

    @profile.crear = params[:crear];
    @profile.editar = params[:editar];
    @profile.leer = params[:leer];
    @profile.eliminar = params[:eliminar];
    @profile.total = params[:total];


    respond_to do |format|
      if @profile.update(profile_params)
        format.html { redirect_to @profile, notice: 'Profile was successfully updated.' }
        format.json { render :show, status: :ok, location: @profile }
      else
        format.html { render :edit }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /profiles/1
  # DELETE /profiles/1.json
  def destroy
    @profile.destroy
    respond_to do |format|
      format.html { redirect_to profiles_url, notice: 'Profile was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_profile
    @profile = Profile.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def profile_params
    params.require(:profile).permit(:name, :crear, :editar, :leer, :eliminar, :total)
  end
end