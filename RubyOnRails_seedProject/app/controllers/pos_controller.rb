class PosController < ApplicationController
  before_action :set_pos, only: [:show, :edit, :update, :destroy]

  # GET /pos
  # GET /pos.json
  def index
    @pos = Pos.all
  end

  # GET /pos/1
  # GET /pos/1.json
  def show
  end

  # GET /pos/new
  def new
    @pos = Pos.new
  end

  # GET /pos/1/edit
  def edit
  end

  # POST /pos
  # POST /pos.json
  def create
    @pos = Pos.new(pos_params)

    respond_to do |format|
      if @pos.save
        format.html { redirect_to @pos, notice: 'Pos was successfully created.' }
        format.json { render :show, status: :created, location: @pos }
      else
        format.html { render :new }
        format.json { render json: @pos.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pos/1
  # PATCH/PUT /pos/1.json
  def update
    respond_to do |format|
      if @pos.update(pos_params)
        format.html { redirect_to @pos, notice: 'Pos was successfully updated.' }
        format.json { render :show, status: :ok, location: @pos }
      else
        format.html { render :edit }
        format.json { render json: @pos.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pos/1
  # DELETE /pos/1.json
  def destroy
    @pos.destroy
    respond_to do |format|
      format.html { redirect_to pos_index_url, notice: 'Pos was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pos
      @pos = Pos.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pos_params
      params.require(:pos).permit(:shop_id, :number)
    end
end
